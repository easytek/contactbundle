<?php

namespace Easytek\ContactBundle\Form;

use Symfony\Bridge\Doctrine\Form\ChoiceList\EntityChoiceList;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\MinLength;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\FormBuilderInterface;

class ContactFormType extends AbstractType
{
	public function buildForm(FormBuilderInterface $builder, array $options)
	{
		$builder
			->add('email', 'email', array('label' => 'Votre email'))
			->add('sujet', 'text', array('label' => 'Sujet'))
			->add('message', 'textarea', array(
				'help_inline' => 'toto',
				'label' => 'Message',
				'attr' => array(
					'placeholder' => 'Entrez votre message ici',
				)
			))
			->add('captcha', 'captcha', array(
				'label' => 'Code de vérification',
				'attr' => array(
					'class' => 'span2',
				)
			))
		;
	}
	
	public function getName()
	{
		return 'easytek_contact_contact';
	}
}
