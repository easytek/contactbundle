<?php

namespace Easytek\ContactBundle\Service;

use Easytek\EcmsBundle\Service\Configuration;
use Easytek\EcmsBundle\Entity\ConfigurationItem;
use Easytek\EcmsBundle\Service\Configurable;

class ContactConfigurable extends Configurable
{
	public function __construct(Configuration $configuration)
	{
		parent::__construct();
		
		$bundle = (string) $this;
		
		//---
		
		$key = 'entreprise';
		$ci = $configuration->getConfigurationItem($bundle, $key);
		
		if($ci->getValue() == null)
			$ci->setValue('Entreprise');
		
		$ci
			->setKey($key)
			->setBundleName($bundle)
			->setType('text')
			->setOptions(array(
				'label' => "Nom de l'entreprise"
			))
		;
		$this->addConfigurationItem($ci);
		
		//---
		
		$key = 'email';
		
		$ci = $configuration->getConfigurationItem($bundle, $key);
		
		if($ci->getValue() == null)
			$ci->setValue('contact@easytek.fr');
		
		$ci
			->setKey($key)
			->setBundleName($bundle)
			->setType('text')
			->setOptions(array(
				'label' => 'Email de contact'
			))
		;
		$this->addConfigurationItem($ci);
		
		//---
		
		$key = 'adresse';
		$ci = $configuration->getConfigurationItem($bundle, $key);
		
		if($ci->getValue() == null)
			$ci->setValue('123 avenue Victor Hugo, 38000 GRENOBLE');
		
		$ci
			->setKey($key)
			->setBundleName($bundle)
			->setType('text')
			->setOptions(array(
				'label' => 'Adresse postale'
			))
		;
		$this->addConfigurationItem($ci);
		
		//---
		
		$key = 'telephone';
		$ci = $configuration->getConfigurationItem($bundle, $key);
		
		if($ci->getValue() == null)
			$ci->setValue('02.38.00.00.00');
		
		$ci
			->setKey($key)
			->setBundleName($bundle)
			->setType('text')
			->setOptions(array(
				'label' => 'Téléphone'
			))
		;
		$this->addConfigurationItem($ci);
		
		//---
		
		$key = 'titre_page';
		$ci = $configuration->getConfigurationItem($bundle, $key);
		
		if($ci->getValue() == null)
			$ci->setValue('Contactez-nous');
		
		$ci
			->setKey($key)
			->setBundleName($bundle)
			->setType('text')
			->setOptions(array(
				'label' => "Titre de la page"
			))
		;
		$this->addConfigurationItem($ci);
	}
	
	public function __toString()
	{
		return 'contact';
	}
}