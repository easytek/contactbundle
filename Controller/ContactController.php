<?php

namespace Easytek\ContactBundle\Controller;

use Easytek\ContactBundle\Entity\Message;
use Symfony\Component\HttpFoundation\Session;
use Easytek\ContactBundle\Form\ContactFormType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\MinLength;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\NotBlank;

class ContactController extends Controller
{
	/**
	 * @Route("/contact", name="ecms_contact")
	 * @Route("/{_locale}/contact", name="ecms_i18n_contact")
	 * @Template("EasytekContactBundle:Contact:index.html.twig")
	 */
	public function indexAction()
	{
		$request = $this->get('request');

		$form = $this->createContactForm();
		
		$message = $form->getData();
		
		if ($request->getMethod() == 'POST') {
			$form->handleRequest($request);
			$flashBag = $this->get('session')->getFlashBag();

			if ($form->isValid()) {
				$expediteur = $this->get("ecms.configuration")->get('general', 'email_exp');
				$destinataire = $this->get("ecms.configuration")->get('general', 'email_dest');

				$swiftMessage = \Swift_Message::newInstance()
					->setSubject($message->getSujetFormated())
					->setFrom($expediteur)
					->setTo($destinataire)
					->setBody($message->getMessageFormated())
				;
				
				$this->get('mailer')->send($swiftMessage);
				
				$form = false;
				
				$flashBag->add('success', 'Votre message a bien été envoyé.');
			} else {
				$flashBag->add('error', 'Le formulaire contient des erreurs.');
			}
		}

		return array(
			'email' => $this->get("ecms.configuration")->get('contact', 'email'),
			'adresse' => $this->get("ecms.configuration")->get('contact', 'adresse'),
			'telephone' => $this->get("ecms.configuration")->get('contact', 'telephone'),
			'entreprise' => $this->get("ecms.configuration")->get('contact', 'entreprise'),
			'titre_page' => $this->get("ecms.configuration")->get('contact', 'titre_page'),
			'form' => $form ? $form->createView() : $form,
		);
	}
	
	/**
	 * Retourne le formulaire de contact
	 * @return Symfony\Component\Form\Form
	 */
	protected function createContactForm()
	{
		$message = new Message();
		$message->setSujetPrefix('['.$this->get("ecms.configuration")->get('Général', 'title').'] ');
		
		return $this->createForm(new ContactFormType(), $message);
	}
}
