$(document).ready(function(){
	$("#contact-map").gmap3({
		action: 'addMarker',
		address: $(this).attr('data-address'),
		map:{
			center: true,
			zoom: 13
		},
		marker: {
			options: { draggable: false }
		},
		infowindow: {
			options: { content: $(this).attr('data-company') },
			events:{ closeclick: function(){} }
		}
	});
});