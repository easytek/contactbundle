<?php

namespace Easytek\ContactBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class Message
{
	/**
	 * @var string
	 * @Assert\Email()
	 */
	protected $email;
	
	/**
	 * @var string
	 * @Assert\NotBlank()
	 */
	protected $sujet;
	
	/**
	 * @var string
	 */
	protected $sujetPrefix;
	
	/**
	 * @var string
	 * @Assert\NotBlank()
	 */
	protected $message;
	
	public function getEmail()
	{
		return $this->email;
	}
	
	public function setEmail($email)
	{
		$this->email = $email;
		return $this;
	}
	
	public function getSujet()
	{
		return $this->sujet;
	}
	
	public function setSujet($sujet)
	{
		$this->sujet = $sujet;
		return $this;
	}
	
	public function setSujetPrefix($prefix)
	{
		$this->sujetPrefix = $prefix;
	}
	
	public function getSujetPrefix()
	{
		return $this->sujetPrefix;
	}
	
	public function getMessage()
	{
		return $this->message;
	}
	
	public function setMessage($message)
	{
		$this->message = $message;
		return $this;
	}
	
	public function getSujetFormated()
	{
		return $this->getSujetPrefix().$this->getSujet();
	}
	
	public function getMessageFormated()
	{
		$message = '';
		
		$message .= 'Email : '.$this->getEmail()."\n\n";
		$message .= "\n\n";
		$message .= 'Message : '."\n\n".$this->getMessage()."\n\n";
		
		return $message;
	}
}
