<?php

namespace Easytek\ContactBundle\Service;

use Easytek\EcmsBundle\Service\Linkable;

class ContactLinkable extends Linkable
{
	public function __construct($configuration)
	{
		$this->configuration = $configuration;
		
		$this->publicName = 'Contact';
		
		$this->addRoute($this->configuration->get('contact', 'titre_page'), 'ecms_contact');
		
		$this->addRoute($this->configuration->get('contact', 'titre_page') . ' (i18n)', 'ecms_i18n_contact');
	}
}