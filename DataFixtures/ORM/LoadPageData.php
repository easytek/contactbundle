<?php

namespace Easytek\ContactBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Easytek\EcmsBundle\Entity\Page;

class LoadPageData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
    	$pages = array();
    	
    	$page = new Page();
    	$page
	    	->setTitre('contact_titre')
    		->setHtml("<h2>Contactez-nous</h2>")
    		->setEmbeded(true)
    		->setLinkable(false)
    	;
    	
    	$pages[] = $page;
    	
        $page = new Page();
        $page
        	->setTitre('contact')
        	->setHtml("<p>Pour nous contacter, vous pouvez soit remplir le formulaire ci-dessous, soit envoyez un email &agrave; <a href=\"mailto:contact@easytek.fr\">contact@easytek.fr</a></p>
						<p>Entreprise<br />123 avenue Victor Hugo, 38000 GRENOBLE<br />T&eacute;l. 02.38.00.00.00</p>
        	")
        	->setEmbeded(true)
        	->setLinkable(false)
        ;
        
        $pages[] = $page;

        foreach($pages as $page)
	        $manager->persist($page);
        
        $manager->flush();
    }
    
    public function getOrder()
    {
    	return 2;
    }
}