<?php

namespace Easytek\ContactBundle\DataFixtures\ORM;

use Easytek\EcmsBundle\Entity\MenuItem;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Easytek\EcmsBundle\Entity\Page;

class LoadMenuData extends AbstractFixture implements OrderedFixtureInterface
{
	public function load(ObjectManager $manager)
    {
    	$menuItem = new MenuItem();
    	$menuItem
    		->setName('Contactez-nous')
    		->setUri('/contact')
    		->setParent($this->getReference('rootMenuItem'))
		;

        $manager->persist($menuItem);
        $manager->flush();
    }
    
    public function getOrder()
    {
    	return 2;
    }
}