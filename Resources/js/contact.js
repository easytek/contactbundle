$(document).ready(function(){
	$(".page-contact .map").each(function(){
        if (typeof contactMarkers === 'undefined') {
            contactMarkers = [];
        }

        if ($(this).data('marker-address') && $(this).data('marker-data')) {
            contactMarkers.push({
                data: $(this).data('marker-data'),
                address: $(this).data('marker-address')
            })
        }

        console.log(contactMarkers);

        if (contactMarkers.length == 0) {
            return;
        }

        if (contactMarkers.length == 1) {
            $(this).gmap3({
                marker:{
                    address: contactMarkers[0].address
                },
                map:{
                    options:{
                        zoom: 14
                    }
                }
            });
        } else {
            $(this).gmap3({
                marker: {
                    values: contactMarkers,
                    options:{
                        draggable: false
                    },
                    events:{
                        mouseover: function(marker, event, context){
                            var map = $(this).gmap3("get");
                            var infowindow = $(this).gmap3({get: {name: "infowindow"}});
                            var content = "<div style=\"line-height: 1.35; overflow: hidden; white-space: nowrap;\">" + context.data + "</div>";

                            if (infowindow){
                                infowindow.open(map, marker);
                                infowindow.setContent(content);
                            } else {
                                $(this).gmap3({
                                    infowindow:{
                                        anchor: marker,
                                        options: {
                                            content: content,
                                            maxWidth: 500
                                        }
                                    }
                                });
                            }
                        }
                    }
                },
                autofit: {}
            });
        }
    });

});